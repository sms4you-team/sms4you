SOURCES=sms4you sms4you-email sms4you-xmpp

all: sms4you.1 index.html sms4you.png

check:
	black --check --line-length 96 .
	flake8 .

format:
	black --line-length 96 .

sms4you.1: README.rst
	cat $< \
	    | sed -e '0,/^$$/s//:Manual section: 1\n/' \
	    | rst2man \
	    | grep -v '^\[image: ' \
	      > $@

index.html: README.rst
	rst2html $< > $@

sms4you.png: sms4you.svg
	inkscape -w 252 -h 252 $< -o $@

clean:
	rm -f sms4you.1 index.html sms4you.png
