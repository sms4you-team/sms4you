sms4you - Modem configuration
=============================

Filter modems, if there are more than one, e.g.:
    Set `SMS4YOU_MODEM_FILTER` with
        " DeviceIdentifier:44556677[..]ccddeeff,"
        " EquipmentIdentifier:887766545667788,"
        " Manufacturer:A_Company, or Model:ABCD",
