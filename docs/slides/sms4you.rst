:title: sms4you
:css: style.css

.. header::

   sms4you

.. footer::

   2019-10-29

----

Why still SMS in 2019?
======================

- There are people with a dumb phone, not a smart phone.
- Some services (e.g. banks) insist on SMS TAN or verification, despite security and convenience penalties.
- Sometimes there is only GSM available, but no mobile data.
- Least common denominator for mobile texting.

----

Why not use SMS with a phone?
=============================

- There are good reasons not to own or use a mobile phone at all.
- SMS roaming might be expensive in the case of travelling.

Opposite use case:
------------------
- Only dumb phone available or no data connection present.
- But post-SMS communication is needed.

----

``sms4you`` to the rescue!
==========================

- ``sms4you`` is a gateway between SMS and XMPP for a single user, the phone number is used as id.
- Only a USB modem for GSM/UMTS/LTE and a computer with Internet connection is needed.
- XMPP can be used from almost any device, even via a web browser.
- SIM stays at home, tracking becomes pointless.

----

Why not use an existing gateway?
================================

- ``soprani.ca`` a.k.a. ``JMP.chat`` offer a very nice gateway between telephony and XMPP, including SMS.
- Unfortunately, they have trunk phone numbers, that are not accepted for TANs by most banks.
- People complain about costly text messages to Canadian phone numbers.
- For privacy or similar reasons, self-hosting might be preferred.

----

How does it work?
=================

- ``sms4you`` is implemented as XMPP server component.
- Tested with Prosody, should work with any XMPP server.

----

Where can I find it?
====================

- https://salsa.debian.org/sms4you-team/sms4you
- AGPL-3 licensed
