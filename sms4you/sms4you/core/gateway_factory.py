#!/usr/bin/python3

# Copyright (C) 2019-2023
#
# * Felix Delattre <felix@delattre.de>
# * W. Martin Borgert <debacle@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import importlib
from .gateway_sms import GatewaySms


class GatewayFactory(object):
    def __init__(self, config, loop, logger):
        self.config = config
        self.loop = loop
        self.logger = logger
        self.selector = self.config.target_gateway

    def get_sms_gateway(self):
        try:
            return GatewaySms(self.config, self.loop, self.logger)
        except ImportError:
            sys.stderr.write("Error: No gateway found for SMS\n")
            sys.exit()

    def get_other_gateway(self):
        selector = self.selector

        try:
            module = importlib.import_module(
                "sms4you-" + selector, package="sms4you-" + selector
            )
            other_gateway = getattr(module, "Gateway" + selector.capitalize())
            return other_gateway(self.config, self.loop, self.logger)
        except ImportError:
            sys.stderr.write("Error: No gateway found for " + selector + "\n")
            sys.exit()
