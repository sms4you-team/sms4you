#!/usr/bin/python3

# Copyright (C) 2019-2023
#
# * Felix Delattre <felix@delattre.de>
# * W. Martin Borgert <debacle@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from setuptools import setup, find_packages

with open(os.path.join("..", "VERSION")) as version_file:
    version = version_file.read().strip()

setup(
    name="sms4you",
    version=version,
    description="Personal gateway connecting SMS with XMPP or email",
    long_description="Personal gateway connecting SMS with XMPP or email",
    url="https://salsa.debian.org/sms4you-team/sms4you",
    license="AGPLv3+",
    keywords="sms email gateway",
    author="Various collaborators: https://salsa.debian.org/sms4you-team/sms4you",
    install_requires=["sdnotify", "systemd-python"],
    packages=find_packages(),
    include_package_data=True,
    entry_points={"console_scripts": ["sms4you = sms4you.sms4you:main"]},
    python_requires=">=3.6",
)
